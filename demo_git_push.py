from git import Repo
from datetime import datetime
import os

folder_path = r"C:\Users\admin\Desktop\Test_git_push\test_sink\demo6"

# URL of the Git repository
repo_url = "https://gitlab.com/Suraj_06/test_sink.git"

try:
    # Initialize the repository (create if it doesn't exist)
    repo = Repo.init(folder_path)

    # Add untracked files and changes to the staging area
    repo.index.add(repo.untracked_files)

    # Create a descriptive commit message
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")  # Format for readability
    commit_message = f"Uploaded folder via Airflow at {now}"
    repo.index.commit(commit_message)

    # Handle remote creation gracefully
    try:
        # Attempt to access the "origin" remote
        origin = repo.remote("origin")
    except AttributeError:  # Catch more general attribute error
        # Create the remote if it doesn't exist
        origin = repo.create_remote("origin", repo_url)

    # Push changes to remote repository, specifying the branch ("main")
    origin.push("--set-upstream", "main")

except Exception as e:
    print(f"An error occurred: {e}")
