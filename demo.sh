#!/bin/bash

# Define variables
REPO_URL="https://gitlab.com/Suraj_06/test_sink.git"
FOLDER_TO_UPLOAD="demo11"

# Check if Git is installed
if ! command -v git >/dev/null 2>&1; then
  echo "Error: Git is not installed. Please install Git before running this script."
  exit 1
fi

# Navigate to the folder containing the files to upload (modify if needed)


# Initialize a new Git repository if not already initialized

# Add all files and subdirectories recursively in the specified folder
#git add -A "*"
git add .

# Optionally add a more informative commit message if desired
git commit -m "Added files and directories from $FOLDER_TO_UPLOAD"

# Add the remote repository
#git remote add origin "$REPO_URL"

# Push the changes to the remote repository using SSH authentication (preferred)
git push -u origin main

# Alternative: Push using HTTPS authentication (if SSH is not set up)
# git push -u origin main --set-upstream

# (Optional) Verify remote repository status (uncomment if desired)
# git ls-remote origin
